﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;
using System.IO;
using System.Linq;
using UnityEngine.Video;

namespace Doha {

public class DohaMaster : MonoBehaviour {

    private static DohaMaster instance;
    public static DohaMaster Get( ) { return instance; }
    void Awake( ) { instance = this; }

    public GameObject projectIdDialog;
    public InputField projectIdInput;
    public Doha.Video videoPrefab;
    public Transform videoContainer;
    public MeshRenderer[ ] outlines;
    public Button confirmButton;
    public VideoPlayer[ ] displays;
    public Text projectIdText;

    string projectId = "";
    JSONObject config = null;
    Doha.Video selectedVideo = null;
    JSONObject tempConfig = null;
    int tempStart = 0;
    int tempEnd = 0;

    const string SEGMENTS = "segments";
    const string START = "start";
    const string END = "end";
    const string URL = "url";
    const string PROJECT_ID = "projectId";

    void Start( ) {

        CloseProjectIdDialog( );
        LoadVideos( );
        LoadFromCache( );
        SetSelectMode( false );
    }

    void Update( ) {

        projectIdText.text = projectId;
    }

    string CachePath( ) {

        return Path.Combine( Application.persistentDataPath, "cache.json" );
    }

    string VideoPath( ) {

        return Application.streamingAssetsPath;
    }

    void LoadFromCache( ) {

        if( File.Exists( CachePath( ))) {

            var text = File.ReadAllText( CachePath( ));
            var cache = JSON.Parse( text ) as JSONObject;
            Debug.Log( $"found in cache: { cache[ PROJECT_ID ] }" );
            SetProjectId( cache[ PROJECT_ID ]);
        }
        else {

            OpenProjectIdDialog( );
        }
    }

    void SaveToCache( ) {

        var cache = new JSONObject( );
        cache[ PROJECT_ID ] = projectId;
        File.WriteAllText( CachePath( ), cache.ToString( ));
    }

    public void OpenProjectIdDialog( ) {

        projectIdDialog.SetActive( true );
        AbortSelection( );
    }

    void CloseProjectIdDialog( ) {

        projectIdDialog.SetActive( false );
    }

    public void SetProjectId( string id ) {

        projectId = id;
        SaveToCache( );
        LoadConfig( );
    }
 
    public void ReadProjectId( ) {

        SetProjectId( projectIdInput.text );
    }

    public void CreateProject( ) {

        var id = System.Guid.NewGuid( ).ToString( ).Substring( 0, 5 );
        projectId = id;
        config = GetNewConfig( );
        SaveConfig( );
        SaveToCache( );
        CloseProjectIdDialog( );
        ImplementConfig( config );
    }

    JSONObject GetNewConfig( ) {

        var config = new JSONObject( );
        var segments = new JSONArray( );
        
        for( int i = 0; i < 5; ++ i ) {
            
            segments.Add( GetNewSegment( i, i, "" ));
        }

        config[ SEGMENTS ] = segments;
        return config;
    }

    JSONObject GetNewSegment( int start, int end, string url ) {

        var segment = new JSONObject( );
        segment[ START ] = start;
        segment[ END ] = end;
        segment[ URL ] = url;

        return segment;
    }

    void SaveConfig( ) {

        StartCoroutine( UploadConfig( ));
    }

    void LoadConfig( ) {

        StartCoroutine( DownloadConfig( ));
    }

    IEnumerator DownloadConfig( ) {

        var r = UnityWebRequest.Get( $"http://duddels.de/doha/{ projectId }" );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) {

            OpenProjectIdDialog( );
            Debug.Log( r.error );
        }
        else {

            config = JSON.Parse( r.downloadHandler.text ) as JSONObject;
            CloseProjectIdDialog( );
            Debug.Log( "download successful" );
            ImplementConfig( config );
        }
    } 

    IEnumerator UploadConfig( ) {

        var form = new WWWForm( );
        form.AddField( "file", projectId );
        form.AddField( "data", config.ToString( ));

        var r = UnityWebRequest.Post( "http://duddels.de/doha/set.php", form );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) Debug.Log( r.error );
        else Debug.Log( "upload successful" );
    }

    void LoadVideos( ) {

        var files = Directory.GetFiles( VideoPath( ));
        var videos = files.Where( f => ! f.EndsWith( ".meta" )).ToList( );

        foreach( var path in videos ) {

            var video = Instantiate( videoPrefab );
            video.url = Path.Combine( VideoPath( ), path );
            video.displayName = Path.GetFileName( path );
            video.transform.SetParent( videoContainer );
        }
    }

    //OUTLINES

    void SetSelectMode( bool enabled ) {

        if( ! enabled && selectedVideo != null ) {

            tempConfig = null;
            selectedVideo.MarkActive( false );
            selectedVideo = null;
            tempStart = -10;
            tempEnd = -10;
        }

        if( enabled ) {

            tempConfig = JSON.Parse( config.ToString( )) as JSONObject;
            selectedVideo.MarkActive( true );
            tempStart = -10;
            tempEnd = -10;
        }

        foreach( var o in outlines ) {

            o.enabled = enabled;
            o.material.color = Color.white;
        }

        confirmButton.gameObject.SetActive( enabled );

        foreach( var poiSphere in GameObject.FindObjectsOfType< Well.PointOfInterestSphere >( )) {

            poiSphere.GetComponent< Collider >( ).enabled = ! enabled;
        }
    }

    void ImplementConfig( JSONObject config ) {

        if( config == null ) return;

        foreach( var display in displays ) {

            display.transform.parent.gameObject.SetActive( false );
        }

        foreach( JSONNode segment in config[ SEGMENTS ]) {

            SetSegment( segment[ START ], segment[ END ], segment[ URL ]);
        }
    }

    int GetDisplayIndex( int start, int end ) {

        int baseIndex = ( new int[ ]{ 0, 5, 9, 12, 14 })[ end - start ];
        return baseIndex + start;
    }

    void SetSegment( int start, int end, string url ) {

        var correctedUrl = url == "" ? null : url;

        var i = GetDisplayIndex( start, end );

        if( correctedUrl != null ) displays[ i ].url = correctedUrl;
        displays[ i ].transform.parent.gameObject.SetActive( correctedUrl != null );
    }

    public void OnVideoClick( Doha.Video video ) {

        if( selectedVideo ) AbortSelection( );
        selectedVideo = video;
        SetSelectMode( true );
    }

    public void OnConfirmClick( ) {

        config = tempConfig;
        ImplementConfig( config );
        SaveConfig( );
        SetSelectMode( false );
    }

    void AbortSelection( ) {

        ImplementConfig( config );
        SetSelectMode( false );
    }

    public void OnOutlineSelect( Outline outline ) {

        if( outline.index == tempStart - 1 ) {

            tempStart = outline.index;
        }
        else if( outline.index == tempEnd + 1 ) {

            tempEnd = outline.index;
        }
        else {

            tempStart = tempEnd = outline.index;

            foreach( var o in outlines ) {

                o.material.color = Color.white;
            }
        }

        outline.GetComponent< MeshRenderer >( ).material.color = new Color( 0, 1f, 0 );
        RectifyConfig( );
        ImplementConfig( tempConfig );
    }

    void RectifyConfig( ) {

        var segmentsNew = new JSONArray( );
        segmentsNew.Add( GetNewSegment( tempStart, tempEnd, selectedVideo.url ));

        foreach( JSONNode segment in tempConfig[ "segments" ]) {

            var start = segment[ "start" ];
            var end = segment[ "end" ];

            if( tempStart <= end && start <= tempEnd ) {

                //A
            }
            else if( start <= tempEnd && tempStart <= end ) {

                //B
            }
            else if( tempStart <= start && end <= tempEnd ) {

                //C
                //nothing to do here
            }
            else if( start <= tempStart && tempEnd <= end ) {

                //D
            }
            else {

                //no overlap
                segmentsNew.Add( segment );
            }
        }

        tempConfig[ "segments" ] = segmentsNew;
    }
}


}

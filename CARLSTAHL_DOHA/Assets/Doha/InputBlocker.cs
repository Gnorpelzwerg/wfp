﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Doha {


public class InputBlocker : MonoBehaviour {

    public Well.Input target;

    public void OnMouseEnter( ) {

        target.enabled = false;
    }

    public void OnMouseLeave( ) {

        target.enabled = true;
    }
}


}

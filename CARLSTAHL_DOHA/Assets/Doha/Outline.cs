﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Doha {


public class Outline : MonoBehaviour {

    public MeshRenderer meshRenderer;
    public int index;
    public float selectedWidth;
    public float unselectedWidth;

    void Start( ) {

        SetWidth( unselectedWidth );
    }

    void OnMouseDown( ) {

        OnDown( );
    }

    void OnMouseEnter( ) {

        SetWidth( selectedWidth );
    }

    void OnMouseExit( ) {

        SetWidth( unselectedWidth );
    }

    void SetWidth( float width ) {

        meshRenderer.material.SetFloat( "_Width", width );
    }

    public void OnDown( ) {

        DohaMaster.Get( ).OnOutlineSelect( this );
    }
}


}
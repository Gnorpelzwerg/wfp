﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Doha/Outline" {
   
    Properties {

        _Width( "Width", Range( 0, 1000 )) = 1
        _Color( "Color", Color ) = ( 1, 1, 1, 1 )
    }
    SubShader {
      
        Tags { 

            "RenderType" = "Opaque"
        }

        ZWrite On
        Cull Front

        Pass {   
         
            CGPROGRAM

            #pragma vertex vert  
            #pragma fragment frag
            #include "UnityCG.cginc"

            float _Width;
            float4 _Color;

            struct w2v {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f {
                float4 pos : SV_POSITION;
            };

            v2f vert( w2v v ) {

                v2f o;
                float dist = length( UnityObjectToClipPos( float4( 1, 0, 0, 0 )));
                o.pos = mul( UNITY_MATRIX_M, v.vertex ) + mul( UNITY_MATRIX_M, float4( v.normal, 0 ) * _Width * dist );
                o.pos = UnityWorldToClipPos( o.pos );

                return o;
            }

            float4 frag( v2f i ) : SV_Target {
                
                return _Color;
            }

            ENDCG
        }
    }
}
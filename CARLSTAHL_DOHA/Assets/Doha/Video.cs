﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Doha {


public class Video : MonoBehaviour {
    
    public string url;
    public string displayName;

    public Text text;

    void Start( ) {

        text.text = $"• { displayName }";
    }

    public void OnClick( ) {

        DohaMaster.Get( ).OnVideoClick( this );
    }

    public void MarkActive( bool active ) {

        text.text = active ? $"• <color=#0f0>{ displayName }</color>" : $"• { displayName }";
    }
}


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {
    
    public Slider bar;
    public Text text;
    public Text progressText;
    public Transform wheel;

    [Space( 10 )]
    public string sceneToLoadName = "main";
    public string loadingText = "Loading, please wait...";
    public string activationText = "Starting...";
    AsyncOperation op;

    void Start( ) {

        Application.backgroundLoadingPriority = ThreadPriority.Low;
    }

    void Update( ) {

        float progress = 0;

        if( op != null ) progress = op.progress;

        bar.value = progress;
        text.text = progress >= 0.9f ? activationText : loadingText;
        progressText.text = "" + (int)( progress * 100 ) + "%";
        wheel.Rotate( Vector3.forward, - Time.deltaTime * 360f );

        if( progress >= 0.9f ) {

            op.allowSceneActivation = true;
        }

        if( op == null ) {

            op = SceneManager.LoadSceneAsync( sceneToLoadName );
            op.allowSceneActivation = false;  
        }
    }
}

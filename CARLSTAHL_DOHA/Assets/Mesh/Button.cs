﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;

namespace Mesh {

	public enum Property { pitch, timeOfDay, urban, scene, distanceUnit, LED };

	public class Button : Listener {

		public Image normalImage;
		public Image selectedImage;
		public Toggle toggle;

    	public Sprite normal;
    	public Sprite selected;

    	public void Modify( ) {

    		var master = FindObjectOfType< MeshMaster >( );
    		if( master ) master.SetProperty( representProperty, propertyValue, toggle.isOn );
    	}

        protected override void OnPropagateListen( Property property, string v ) {

            toggle.isOn = propertyValue.Equals( v );
        }
	}

	#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Button ))]
    public class ButtonInspector : ListenerInspector {

        protected override void OnSmartInspectorGUI( Listener target ) {

            var b = target as Button;

        	ExposeInternalReference( "normalImage" );
            ExposeInternalReference( "selectedImage" );
            ExposeInternalReference( "toggle" );

        	Expose( "normal" );
        	b.normalImage.sprite = b.normal;

        	Expose( "selected" );
        	b.selectedImage.sprite = b.selected;

        	base.OnSmartInspectorGUI( target );

            if( target.representProperty != Property.urban && target.representProperty != Property.LED ) {

                Expose( "propertyValue" );
            }
            else {

                target.propertyValue = "1";
            }   

            if( target.representProperty == Property.scene || target.representProperty == Property.distanceUnit ) {

                Warning( "This property must not be modified by a button" );
            }
        }
    }
    
    #endif
}



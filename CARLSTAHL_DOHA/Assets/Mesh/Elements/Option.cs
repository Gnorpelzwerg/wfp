﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;
using UnityEngine.Video;

namespace Mesh {

	public class Option : Listener {
 
        public Well.Scene scene;
        public DistanceUnit distanceUnit;
        public string clipVersion = "";

        protected bool shouldRepresent = false;

        public bool ShouldRepresent( ) {

            return shouldRepresent;
        }

		protected override void OnPropagateListen( Property property, string v ) {

			if( property == Property.scene ) {

                //this is a bit risky
                propertyValue = ( scene != null ) ? scene.GetInstanceID( ).ToString( ) : "";
            }

            if( property == Property.urban || property == Property.LED ) {

                propertyValue = "1";
            }

            if( property == Property.distanceUnit ) {

                propertyValue = distanceUnit.ToString( );
            }

            shouldRepresent = propertyValue.Equals( v );

            bool enable = true;
            foreach( var option in GetComponents< Option >( )) {

                enable &= option.ShouldRepresent( );
            }

            foreach( var renderer in GetComponentsInChildren< MeshRenderer >( )) {

                renderer.enabled = enable;
            }

            foreach( var light in GetComponentsInChildren< Light >( )) {

                light.enabled = enable;
            }

            foreach( var canvas in GetComponentsInChildren< Canvas >( )) {

                canvas.enabled = enable;
            }
        }
	}

	#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Option ))]
    public class OptionInspector : ListenerInspector {

        protected override void OnSmartInspectorGUI( Listener target ) {
            
      		base.OnSmartInspectorGUI( target );

            var o = target as Option;

            switch( o.representProperty ) {

                case Property.urban :
                case Property.LED : break;
                case Property.scene : Expose( "scene", "Property Value" ); break;
                case Property.distanceUnit : Expose( "distanceUnit", "Property Value" ); break;
                case Property.pitch :
                case Property.timeOfDay : Expose( "propertyValue" ); break;
            }
        }
    }
    
    #endif
}



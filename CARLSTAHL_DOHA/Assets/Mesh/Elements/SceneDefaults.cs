﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEditor;
using SmartEditor;

namespace Mesh {

    public class SceneDefaults : MonoBehaviour {

    	public Well.Scene scene;
        public Transform target;
        public string pitch;
        public string timeOfDay;
        public bool urban;
        public bool LED;
    }

    #if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( SceneDefaults ))]
    public class SceneDefaultsInspector : SmartEditor< SceneDefaults > {

        protected override void OnSmartInspectorGUI( SceneDefaults target ) {

        	Expose( "scene", "Defaults For Scene" );
            Expose( "target" );
            Expose( "pitch" );
            Expose( "timeOfDay" );
            Expose( "urban" );
            Expose( "LED" );
        }
    }
    
    #endif
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;

namespace Mesh {

	public class Listener : MonoBehaviour {

		public Property representProperty;
		public string propertyValue;

    	public void Register( ) {

    		var master = FindObjectOfType< MeshMaster >( );
    		if( master ) master.Register( this );
    	}

    	public void PropagateListen( Property property, string v ) {

    		if( property == representProperty ) {

    			OnPropagateListen( property, v );
    		}
    	}

    	protected virtual void OnPropagateListen( Property Property, string v ) { }

    	void Start( ) {

    		Register( );
    	}
	}

	#if UNITY_EDITOR
    
    public class ListenerInspector : SmartEditor< Listener > {

        protected override void OnSmartInspectorGUI( Listener target ) {

        	Expose( "representProperty" );     
        }
    }
    
    #endif
}



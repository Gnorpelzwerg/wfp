﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEditor;
using SmartEditor;

namespace Mesh {

    public enum DistanceUnit { meter = 0, feet = 1 };

    public class MeshMaster : MonoBehaviour {

    	public Text distance;

        public Well.WellMaster wellMaster;
        public SceneDefaults currentScene = null;

    	public DistanceUnit distanceUnit = DistanceUnit.meter;
        public Collider targetCollider;

        List< Listener > listeners = null;

        public void Register( Listener listener ) {

            if( listeners == null ) {

                listeners = new List< Listener >( );
                currentScene = GetSceneDefaults( wellMaster._defaultScene );
            }

            listeners.Add( listener );
            PropagateAll( );
        }

        public void SetProperty( Property property, string v, bool isOn ) {

            if( isOn ) {

                switch( property ) {

                    case Property.pitch : SetPitch( v ); break;
                    case Property.timeOfDay : SetTimeOfDay( v ); break;
                }
            }
            if( property == Property.urban ) {

                SetUrban( isOn );
            }
            if( property == Property.LED ) {

                SetLED( isOn );
            }

            PropagateAll( );
        }

        public void SetPitch( string v ) { currentScene.pitch = v; }
        public void SetUrban( bool v ) { currentScene.urban = v; }
        public void SetLED( bool v ) { currentScene.LED = v; }

        public void SetTimeOfDay( string v ) { currentScene.timeOfDay = v; }

        public void PropagateAll( ) {

            Propagate( Property.pitch, currentScene.pitch );
            Propagate( Property.timeOfDay, currentScene.timeOfDay );
            Propagate( Property.urban, currentScene.urban ? "1" : "0" );
            Propagate( Property.LED, currentScene.LED ? "1" : "0" );
            var scene = wellMaster.GetActiveScene( );
            Propagate( Property.scene, ( scene != null ) ? scene.GetInstanceID( ).ToString( ) : "" );
            Propagate( Property.distanceUnit, distanceUnit.ToString( ));
        }

        public void Propagate( Property property, string v ) {

            if( listeners != null )
            foreach( var listener in listeners ) {

                listener.PropagateListen( property, v );
            }
        }

        public void SwitchDistanceUnit( ) {

        	distanceUnit = (DistanceUnit) ((( (int) distanceUnit ) + 1 ) % 2 );
            PropagateAll( );
        }

        void Update( ) {

        	float distMeters = ( targetCollider.ClosestPointOnBounds( Camera.main.transform.position ) - Camera.main.transform.position ).magnitude;
        	distance.text = distanceUnit == 0 ? DistanceInMeters( distMeters ) : DistanceInFeet( distMeters );

            var d = GetSceneDefaults( wellMaster.GetActiveScene( ));
            bool changed = d != currentScene;
            currentScene = d;
            if( changed ) PropagateAll( );
        }

        string DistanceInMeters( float meters ) { return (int) meters + " m"; }
        string DistanceInFeet( float meters ) { return (int)(meters*3.281f) + " ft"; }

        SceneDefaults GetSceneDefaults( Well.Scene scene ) {

            foreach( SceneDefaults defaults in GameObject.FindObjectsOfType< SceneDefaults >( )) {

                if( defaults.scene == scene ) return defaults;
            }

            return null;
        }
    }

    #if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( MeshMaster ))]
    public class MeshMasterInspector : SmartEditor< MeshMaster > {

        protected override void OnSmartInspectorGUI( MeshMaster target ) {

            ExposeInternalReference( "distance" );
            ExposeInternalReference( "target" );
            ExposeInternalReference( "targetCollider" );

            Expose( "wellMaster" );

            if( target.wellMaster == null ) return;

            foreach( Well.Scene scene in target.wellMaster._scenes ) {

                int found = 0;
                foreach( SceneDefaults defaults in GameObject.FindObjectsOfType< SceneDefaults >( )) {

                    found += defaults.scene == scene ? 1 : 0;
                }

                if( found == 0 ) Warning( "A SceneDefaults Component is required for Scene '" + scene._displayName + "'" );
                if( found > 1 ) Warning( "Multiple SceneDefaults Components existing for Scene '" + scene._displayName + "'" );
            }
        }
    }
    
    #endif
}


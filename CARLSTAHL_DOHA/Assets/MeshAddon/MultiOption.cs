﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;
using UnityEngine.Video;
using Mesh;
using System;

namespace MeshAddon {

    public class MultiOption : Option {

        protected override void OnPropagateListen( Property property, string v ) {

            //warm up the propertyValue
            base.OnPropagateListen( property, v );

            var moddedValue = v;
            foreach( var split in propertyValue.Split( '|' )) {

                var option = split.Trim( );
                if( option.Equals( v )) {

                    moddedValue = propertyValue;
                }
            }

            //second pass with the matched
            base.OnPropagateListen( property, moddedValue );            
        }
    }

    #if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( MultiOption ))]
    public class MultiOptionInspector : OptionInspector {}
    
    #endif
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SmartEditor {

    #if UNITY_EDITOR
    
    public abstract class SmartEditor< Type > : Editor where Type : MonoBehaviour {
    
            protected bool showInternalSettings = false;
            bool hasInternalSettings;
            bool lastDrawnIsInternal;

            public sealed override void OnInspectorGUI( ) {
                
                serializedObject.Update( );
                
                hasInternalSettings = false;
                OnSmartInspectorGUI( target as Type );
                
                serializedObject.ApplyModifiedProperties( );
            }
            
            bool SmartPropertyField( SerializedObject target, string name, string alias ) {
            
                var displayName = alias == null ? ObjectNames.NicifyVariableName( name ) : alias;
                return EditorGUILayout.PropertyField( target.FindProperty( name ), new GUIContent( displayName ), true );
            }
            
            protected bool Expose( string variableName, string alias = null ) {
            
                if( lastDrawnIsInternal ) {

                    Space( );
                    lastDrawnIsInternal = false;
                }

                return SmartPropertyField( serializedObject, variableName, alias );
            }

            protected void ExposeInternal( string variableName, bool forceShow = false ) {

                if( ! hasInternalSettings ) {

                    showInternalSettings = EditorGUILayout.ToggleLeft( new GUIContent( "Show Internal Fields" ), showInternalSettings );
                    Space( );
                    hasInternalSettings = true;
                }

                if( showInternalSettings || forceShow ) {

                    lastDrawnIsInternal = false;
                    Expose( variableName );
                    lastDrawnIsInternal = true;
                }
            }

            protected void ExposeInternalReference( string variableName ) {

                var prop = serializedObject.FindProperty( variableName );
                if( prop != null && prop.propertyType == SerializedPropertyType.ObjectReference ) {

                    ExposeInternal( variableName, prop.objectReferenceValue == null );
                }
            }

            protected void Note( string message ) {

                EditorGUILayout.HelpBox( new GUIContent( message ));
            }

            protected void Warning( string message ) {

                EditorGUILayout.HelpBox( message, MessageType.Warning );
            }

            protected void Header( string name ) {

                EditorGUILayout.LabelField( name, EditorStyles.boldLabel );
            }

            protected void Space( ) {

                EditorGUILayout.Space( );
            }
            
            protected virtual void OnSmartInspectorGUI( Type target ) {}
    }
    
    #endif
}


﻿Shader "Unlit/Mesh" {
   
    Properties {

      _Alpha( "Mesh Texture (A)", 2D ) = "white" {}
      _Video( "Video Texture", 2D ) = "white" {}
      _Pitch( "Pitch in mm", Range( 10, 1000 ) ) = 100
      _DistanceF( "Boost/(Pitch x Distance)", Range( 0., .001 )) = 0.0005
      _Max( "Max Boost/Pitch", Range( 0, 1 )) = 0.1

    }
    SubShader {
      
        Tags { 

            "Queue" = "Transparent" 
            "RenderType" = "Transparent"
        }

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 

        Pass {   
         
            CGPROGRAM

            #pragma vertex vert  
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _Alpha;     
            sampler2D _Video;
            float4 _Alpha_ST;
            float _DistanceF;
            float _Pitch;
            float _Max;

            struct w2v {
                float4 vertex : POSITION;
                float4 tex : TEXCOORD0;
            };

            struct v2f {
                float4 pos : SV_POSITION;
                float4 tex : TEXCOORD0;
                float4 view : TEXCOORD1;
            };

            v2f vert( w2v v ) {

                v2f o;
                o.pos = UnityObjectToClipPos( v.vertex );
                o.tex = v.tex;
                o.view = float4( length( mul( UNITY_MATRIX_MV, v.vertex )), 1, 1, 1 ); //camera distance

                return o;
            }

            float4 frag( v2f i ) : SV_Target {

                // HOLGER AREA END

                float boost = min( _Max * _Pitch, i.view.x * _DistanceF * _Pitch );
                float4 col = tex2D( _Video, float2( i.tex.xy ));
                float meshAlpha = tex2D( _Alpha, i.tex.xy * _Alpha_ST.xy ).w;
                float value = length( col.xyz ) / sqrt( 3 ); //get brightness of video feed
                float minValue = 0.1;
                float maxValue = 0.9;
                float finalAlpha = meshAlpha * ( 1 + boost ) * smoothstep( minValue, maxValue, value ); //negative multiply a color ramp of brigthness

                // HOLGER AREA END
                
                return col * float4( 1, 1, 1, finalAlpha );   
            }

            ENDCG
        }
    }
}
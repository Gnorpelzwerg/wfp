﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Well {
    
    [RequireComponent( typeof( Camera ))]
    public class PivotCamera : MonoBehaviour {
    
        public Pivot _pivot;
    
    	[HideInInspector] public float _verticalCurrent;
    	[HideInInspector] public float _horizontalCurrent;
    	[HideInInspector] public float _distanceCurrent;
        
    	public void Initialize( ) {
            
    		_verticalCurrent = _pivot._verticalStart;
    		_horizontalCurrent = _pivot._horizontalStart;
    		_distanceCurrent = _pivot._distanceStart;
    	}
        
        public void SetPivot( Pivot pivot ) {
        
            if( null != pivot ) {
                
                _pivot = pivot;
                Initialize( );
            }
        }
    
    	void LateUpdate( ) {
        
    		_verticalCurrent = Mathf.Clamp( _verticalCurrent, _pivot._verticalMin, _pivot._verticalMax );
    		if( _pivot._horizontalRange != 360 ) { _horizontalCurrent = Mathf.Clamp( _horizontalCurrent, - _pivot._horizontalRange / 2, _pivot._horizontalRange / 2 ); }
    		_distanceCurrent = Mathf.Clamp( _distanceCurrent, _pivot._distanceMin, _pivot._distanceMax );
    
    		transform.position = _pivot.MinY( _pivot.Position( _verticalCurrent, _horizontalCurrent, _distanceCurrent ));
            transform.LookAt( _pivot.transform.position, _pivot.transform.up );
    	}
    }
 }
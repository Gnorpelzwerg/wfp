﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Well {

    public static class Utility {
    
        public static Color _orange = ( Color.red + Color.yellow ) * 0.5f;
    
        #if UNITY_EDITOR
    
        public struct Plane3 {

            public Vector3 _normal;
            public float _d;
            
            public Plane3( Vector3 normal, float d ) {
            
               _normal = normal.normalized;
                _d = d;
            }    
        } 
    
        public static float _fullCircleResolution = 100;
        public static Color _drawColor = Color.green;
        
        public static void DrawLine( Vector3 start, Vector3 end ) {
        
            Gizmos.color = _drawColor;
            Gizmos.DrawLine( start, end );
        }
    
        public static void DrawArcAroundCenter( Vector3 center, Vector3 start, Vector3 end, bool is360 = false ) {
    
            DrawArcCore( center, start, end, Vector3.zero, is360 );
        }
        
        public static void DrawArcAroundAxis( Vector3 center, Vector3 start, Vector3 end, Vector3 axis, bool is360 = false ) {
            
            Plane3 measurePlane = new Plane3( axis.normalized, Vector3.Dot( axis.normalized, start ));
            float distance = Vector3.Dot( center, measurePlane._normal ) - measurePlane._d;
            Vector3 measurePoint = center - measurePlane._normal * distance; 
            
            DrawArcCore( measurePoint, start, end, axis, is360 );
        }
        
        public static LayerMask LayerMaskField( string label, LayerMask selected ) {
 
            List <string > layers = null;
            string[] layerNames = new string[ 4 ];
 
            if( layers == null ) {
             
                layers = new List< string >( );
                layerNames = new string[ 4 ];
            } 
            else {
                 
                layers.Clear ( );
            }
             
            int emptyLayers = 0;
            for( int i = 0; i < 32; i ++ ) {
                 
                string layerName = LayerMask.LayerToName( i );
                 
                if( layerName != "" ) {
                     
                    for( ; emptyLayers > 0 ; emptyLayers -- ) {
                        
                       layers.Add( "UNUSED LAYER " + ( i - emptyLayers ));
                    }
                     
                    layers.Add( layerName );
                     
                } 
                else {
                     
                    emptyLayers ++;
                }
            }
             
            if( layerNames.Length != layers.Count ) {
                 
                layerNames = new string[ layers.Count ];
            }
             
            for( int i = 0; i < layerNames.Length; i ++ ) {
                
                layerNames[ i ] = layers[ i ];  
            }
         
            selected.value = EditorGUILayout.MaskField( label, selected.value, layerNames );
         
            return selected;
        }
        
        static void DrawArcCore( Vector3 center, Vector3 start, Vector3 end, Vector3 aroundAxis, bool is360 ) {
        
            Vector3 startRadius = start - center;
            Vector3 endRadius = end - center;
            Vector3 axis = Vector3.Cross( startRadius.normalized, endRadius.normalized ).normalized;
            float angleTotal = is360 ? 360 : Vector3.SignedAngle( startRadius, endRadius, aroundAxis );
            if( angleTotal < 0 ) { angleTotal = angleTotal + 360; axis = - axis; }
            if( angleTotal % 180 == 0 ) { axis = aroundAxis; }    
            int segments = Mathf.CeilToInt( angleTotal * _fullCircleResolution / 360 );
            float anglePartial = angleTotal / segments;
            
            Vector3 lastVertex = start;
            
            for( int i = 0; i < segments; ++ i ) {
            
                Vector3 vertex = Quaternion.AngleAxis( anglePartial *( i + 1 ), axis ) * startRadius + center;
                DrawLine( lastVertex, vertex );
                lastVertex = vertex;
            }
        }
        
        #endif	
    }

}

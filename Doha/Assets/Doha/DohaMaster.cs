﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;
using System.IO;
using System.Linq;

namespace Doha {


public class DohaMaster : MonoBehaviour {

    public GameObject projectIdDialog;
    public InputField projectIdInput;

    string projectId = "";
    JSONObject config = null;

    void Start( ) {

        CloseProjectIdDialog( );
        LoadVideo( );
        LoadProjectIdFromCache( );
    }

    string CachePath( ) {

        return Path.Combine( Application.persistentDataPath, "cache.json" );
    }

    string VideoPath( ) {

        return Application.streamingAssetsPath;
    }

    void LoadProjectIdFromCache( ) {

        if( File.Exists( CachePath( ))) {

            var text = File.ReadAllText( CachePath( ));
            var cache = JSON.Parse( text ) as JSONObject;
            Debug.Log( $"found in cache: { cache[ "projectId" ] }" );
            SetProjectId( cache[ "projectId" ]);
        }
        else {

            OpenProjectIdDialog( );
        }
    }

    public void OpenProjectIdDialog( ) {

        projectIdDialog.SetActive( true );
    }

    void CloseProjectIdDialog( ) {

        projectIdDialog.SetActive( false );
    }

    public void SetProjectId( string id ) {

        projectId = id;
        var cache = new JSONObject( );
        cache[ "projectId" ] = id;
        File.WriteAllText( CachePath( ), cache.ToString( ));
        LoadConfig( );
    }
 
    public void ReadProjectId( ) {

        SetProjectId( projectIdInput.text );
    }

    public void CreateProject( ) {

        var id = System.Guid.NewGuid( ).ToString( ).Substring( 0, 5 );
        projectId = id;
        config = GetNewConfig( );
        SaveConfig( );
        SetProjectId( id );
    }

    JSONObject GetNewConfig( ) {

        return new JSONObject( );
    }

    void SaveConfig( ) {

        StartCoroutine( UploadConfig( ));
    }

    void LoadConfig( ) {

        StartCoroutine( DownloadConfig( ));
    }

    IEnumerator DownloadConfig( ) {

        var r = UnityWebRequest.Get( $"http://duddels.de/doha/{ projectId }" );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) {

            OpenProjectIdDialog( );
            Debug.Log( r.error );
        }
        else {

            config = JSON.Parse( r.downloadHandler.text ) as JSONObject;
            CloseProjectIdDialog( );
            Debug.Log( "download successful" );
        }
    } 

    IEnumerator UploadConfig( ) {

        var form = new WWWForm( );
        form.AddField( "file", projectId );
        form.AddField( "data", config.ToString( ));

        var r = UnityWebRequest.Post( "http://duddels.de/doha/set.php", form );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) Debug.Log( r.error );
        else Debug.Log( "upload successful" );
    }

    void LoadVideo( ) {

        var files = Directory.GetFiles( VideoPath( ));
        var videos = files.Where( f => ! f.EndsWith( ".meta" )).ToList( );
        GameObject cam = GameObject.Find( "Main Camera" );
        var videoPlayer = cam.AddComponent< UnityEngine.Video.VideoPlayer >( );
        videoPlayer.url = Path.Combine( VideoPath( ), videos[ videos.Count( ) - 1 ]);
        videoPlayer.isLooping = true;
    }
}


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {

public class Dialog : MonoBehaviour {
 
    public Text title;
    public string titleText;
    public string titleTextSaved;
    public string titleTextSent;
    public string titleTextJump;
    public Text description;
    public Text toggleInfo;
    public GameObject okButton;
    public GameObject status;
    public Text checkMark;
    public Color savedColor;
    public Color sentColor;
    public Transform toggleButtonParent;   
    public bool isConfirmDialog; 

    public void Init( Structure.Take take ) {

        gameObject.SetActive( false );
        isConfirmDialog = false;

    	if( take.hasConfirmation || take.hasJump ) {

			gameObject.SetActive( true );
    		description.text = take.message;
    		toggleInfo.gameObject.SetActive( take.GetToggles( ).Length > 0 );

            if( take.hasConfirmation ) {

                isConfirmDialog = true;
            }
    	}
    }

    void Update( ) {

    	if( isConfirmDialog ) {

    		var isSaved = Master.Get( ).IsSaved( );
    		var isSent = Master.Get( ).IsSent( );

    		if( isSent ) {

    			title.text = titleTextSent;
	    		checkMark.color = sentColor;
    		}
    		else if( isSaved ) {

    			title.text = titleTextSaved;
		    	checkMark.color = savedColor;
    		}
    		else {

    			title.text = titleText;
    		}

    		//only update if not saved
    		if( ! isSaved ) toggleInfo.text = GetToggleInfo( );
    		okButton.SetActive( ! isSaved && ! isSent );
    		status.SetActive( isSaved || isSent );
    		if( toggleButtonParent.gameObject.activeSelf && ( isSaved || isSent )) toggleButtonParent.gameObject.SetActive( false );
    	}
        else { //this is a jump dialog

            toggleInfo.text = GetToggleInfo( );
            title.text = titleTextJump;
            okButton.SetActive( true );
            status.SetActive( false );
        }
    }

    string GetToggleInfo( ) {

        var identifiers = new List< string >( );

    	for( int i = 0; i < toggleButtonParent.childCount; ++ i ) {

    		var toggleButton = toggleButtonParent.GetChild( i ).GetComponent< ToggleButton >( );	
    		if( toggleButton.isOn( )) {

                identifiers.Add( toggleButton.toggle.identifier );
    		}
    	}

        identifiers.Sort( );

        var info = "";
        foreach( var identifier in identifiers ) {

            info += " - " + identifier + "\n";
        }

    	return info.TrimEnd( );
    }

    public void OnClick( ) {

    	if( isConfirmDialog ) {

            Master.Get( ).OnConfirm( description.text, GetToggleInfo( ));
        }
        else {

            Master.Get( ).OnJump( );
        }
    }
}


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class DocumentButton : MonoBehaviour {

    public Structure.Document document;
    public Image icon;
    public Image background;
    public Sprite textIcon;
    public Sprite imageIcon;

    public void OnClick( ) {

    	Master.Get( ).OnDocument( document );
    }

    public void MarkActive( bool active ) {

    	background.gameObject.SetActive( active );
    }

    void Update( ) {

    	if( document != null ) {

    		icon.sprite = document.type == Structure.DocumentType.text ? textIcon : imageIcon;	
    	}
    }
}


}
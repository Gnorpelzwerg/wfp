using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Amand {


[ExecuteInEditMode]
public class GridLayoutHelper : MonoBehaviour {

    public Vector2 cellSize;
    public bool dirty = true;

    void Update( ) {

        if( transform.hasChanged ) {

            transform.hasChanged = false;
            dirty = true;
        }

        if( ! Application.isPlaying || dirty ) {

            dirty = false;

            var group = gameObject.GetComponent< GridLayoutGroup >( );
            var n = transform.childCount;

            if( n <= 4 ) {

                group.constraintCount = 1;
                group.cellSize = new Vector2( cellSize.x, cellSize.y * 2 );
            }
            else {

                group.constraintCount = 2;
                group.cellSize = new Vector2( cellSize.x, cellSize.y );
            }    
        }
    }
}


}

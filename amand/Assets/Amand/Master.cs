using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Amand.Structure;
using System.Linq;
using SimpleJSON;
using System;
using System.IO;
using UnityEngine.Networking;

namespace Amand {


public class Record {

    public Take take;
}


public class Master : MonoBehaviour {

	private static Master instance;

	public static Master Get( ) { return instance; }
    
	void Awake( ) { instance = this; }

	public GameObject titleScreen;
	public GameObject menu1;
	public GameObject menu2;
	public GameObject menu3;
	public GameObject mainScreen;
	
	public MenuButton menu2ButtonPrefab;
	public Text menu2Info;
	public Transform menu2Content;
	
	public MenuButton menu3ButtonPrefab;
	public Text menu3Info;
	public Transform menu3Content;
	
	public Text mainScreenTitle;
	public Transform overviewContent;
	public TakeButton takeButtonPrefab;
	public GameObject background; 
	public GameObject leftArrow;
	public GameObject rightArrow;
	public ScreenContent screenContentA;
	public ScreenContent screenContentB;
	public Transform documentParent;
	public DocumentButton documentButtonPrefab;
	public GameObject closeButton;
	public ScrollRect overviewScrollRect;
	public Text overlayText;

	//the only meaningful state is in here
	[Space( 20 )]
	public Structure.Model model;
	public Structure.PerformanceType performanceType;
	public Structure.Performance performance;
	public int takeIndex;
	public Stack< Record > history = new Stack< Record >( );
	public int previousTakeIndex;
	//end

	public ScreenContent activeScreenContent;
	public float lastCheck = 0;
	public HashSet< string > savedUids = new HashSet< string >( );
	public HashSet< string > sentUids = new HashSet< string >( );
	public string currentUid = "dummy";

	public bool IsSaved( ) {

		return savedUids.Contains( currentUid );
	}

	public bool IsSent( ) {

		return sentUids.Contains( currentUid );
	}

	string LogPath( ) {

		return Path.Combine( Application.persistentDataPath );
	}

	void Start( ) {

		SetTitleScreen( );
	}

	void SetTitleScreen( ) {

		titleScreen.SetActive( true );
		menu1.SetActive( false );
		menu2.SetActive( false );
		menu3.SetActive( false );
		mainScreen.SetActive( false );
	}

	void SetMenu1( ) {

		titleScreen.SetActive( false );
		menu1.SetActive( true );
		menu2.SetActive( false );
		menu3.SetActive( false );
		mainScreen.SetActive( false );

		model = null;
		performanceType = null;
		performance = null;
		takeIndex = -1;
		history.Clear( );
	}

	void SetMenu2( ) {

		titleScreen.SetActive( false );
		menu1.SetActive( false );
		menu2.SetActive( true );
		menu3.SetActive( false );
		mainScreen.SetActive( false );

		performanceType = null;
		performance = null;
		takeIndex = -1;
		history.Clear( );

		menu2Info.text = $"<b>{ model.displayName }</b> 〉 Bitte wählen Sie eine Vorgangskategorie aus";
		ClearChildren( menu2Content );

		foreach( Transform child in model.transform ) {

			var performanceType = child.GetComponent< Structure.PerformanceType >( );
			var button = Instantiate( menu2ButtonPrefab, menu2Content );
			button.titleTexts[ 0 ].text = button.titleTexts[ 1 ].text = performanceType.displayName;
			button.menuName = "2";
			button.selection = performanceType;
			button.ImplementSettings( performanceType.buttonSettings );
		}

		menu2Content.GetComponent< GridLayoutHelper >( ).dirty = true;
	}

	void SetMenu3( ) {

		titleScreen.SetActive( false );
		menu1.SetActive( false );
		menu2.SetActive( false );
		menu3.SetActive( true );
		mainScreen.SetActive( false );

		performance = null;
		takeIndex = -1;
		history.Clear( );

		menu3Info.text = $"<b>{ model.displayName }</b> 〉 <b>{ performanceType.displayName }</b> 〉 Bitte wählen Sie einen Vorgang aus";
		ClearChildren( menu3Content );

		foreach( Transform child in performanceType.transform ) {

			var performance = child.GetComponent< Structure.Performance >( );
			var button = Instantiate( menu3ButtonPrefab, menu3Content );
			button.titleTexts[ 0 ].text = button.titleTexts[ 1 ].text = performance.displayName;
			button.menuName = "3";
			button.selection = performance;
			button.ImplementSettings( performance.buttonSettings );
		}

		menu3Content.GetComponent< GridLayoutHelper >( ).dirty = true;
	}

	void SetMainScreen( ) {

		titleScreen.SetActive( false );
		menu1.SetActive( false );
		menu2.SetActive( false );
		menu3.SetActive( false );
		mainScreen.SetActive( true );

		takeIndex = -1;
		
		mainScreenTitle.text = $"<b>{ model.displayName }</b> 〉<b>{ performanceType.displayName }</b> 〉 { performance.displayName.Replace( "-\n", "" ).Replace( "\n", " " )}";
		ClearChildren( overviewContent );

		var takes = GetTakes( );
		for( int i = 0; i < takes.Count; ++ i ) {

			var button = Instantiate( takeButtonPrefab );
			button.transform.SetParent( overviewContent );
			button.label.text = "" + ( i + 1 );
			button.takeIndex = i;
			button.take = takes[ i ];
		}

		activeScreenContent = null;
		SetTake( 0 );
	}

	void SetTake( int newIndex ) {

		if( newIndex == takeIndex ) return;
		PushHistory( );

		var previousTakeIndex = takeIndex;
		takeIndex = newIndex;

		var take = GetCurrentTakeData( );
		leftArrow.SetActive( history.Count > 0 );
		rightArrow.SetActive( ! take.hasJump && ( take.overrideNext || takeIndex < GetTakes( ).Count - 1 ));

		if( activeScreenContent ) {

			activeScreenContent.GetComponent< Animator >( ).SetTrigger( previousTakeIndex < takeIndex ? "fade out left" : "fade out right" );
		}
		
		SwitchScreenContent( );
		activeScreenContent.GetComponent< Animator >( ).SetTrigger( previousTakeIndex < takeIndex ? "fade in right" : "fade in left" );

		if( take.type == Structure.TakeType.scene ) {

			activeScreenContent.SetScene( take.scene );
		}
		else if( take.type == Structure.TakeType.document ) {

			activeScreenContent.SetDocument( take.document );
		}

		ClearChildren( documentParent );

		foreach( var document in take.GetDocuments( )) {

			var button = Instantiate( documentButtonPrefab, documentParent );
			button.document = document;
			button.MarkActive( false );
		}

		closeButton.SetActive( false );
		currentUid = GetUid( );

		overlayText.transform.parent.gameObject.SetActive( take.hasOverlayText );
		overlayText.text = take.overlayText;

		var group = overviewContent.GetComponent< HorizontalLayoutGroup >( );
		var count = GetTakes( ).Count;
		var rt = takeButtonPrefab.GetComponent< RectTransform >( );
		var width = group.padding.left + group.padding.right + ( count - 1 ) * group.spacing + ( count ) * rt.rect.width;
		var positionX = group.padding.left + takeIndex * group.spacing + ( takeIndex + 0.5f ) * rt.rect.width;
		var screenWidth = 2000f;

		var W = width;
		var t = screenWidth / 2;
		var x = positionX;
		
		float y = 0;
		if( x < t ) y = 0;
		else if( x > W - t ) y = 1;
		else y = ( x - t ) / ( W - 2 * t );

		overviewScrollRect.normalizedPosition = new Vector2( y, 0 );
	}

	void PushHistory( ) {

		if( takeIndex != -1 && takeIndex != 1000 ) { //just some large number

			var record = new Record( );
			record.take = GetCurrentTake( );
			history.Push( record );	
		}
	}

	Structure.Take GetCurrentTake( ) { return GetTakes( )[ takeIndex ]; }

	public Structure.Take GetCurrentTakeData( ) { return GetCurrentTake( ).Follow( ); }

	List< Structure.Take > GetTakes( ) {

		return performance.GetComponentsInChildren< Take >( ).OrderBy( c => c.transform.GetSiblingIndex( )).ToList( );
	}

	public static void ClearChildren( Transform parent ) {

		while( parent.childCount > 0 ) {

			var child = parent.GetChild( 0 );
			child.SetParent( null );
			Destroy( child.gameObject );
		}
	}

	void SwitchScreenContent( ) {

		activeScreenContent = activeScreenContent == screenContentA ? screenContentB : screenContentA;
	}

	public void OnMenu( string menuName, UnityEngine.Object selection, string primitiveSelection ) {

		switch( menuName ) {

			case "1": OnMenu1(( selection as GameObject ).GetComponent< Structure.Model >( )); return;
			case "2": OnMenu2( selection as PerformanceType ); return;
			case "3": OnMenu3( selection as Performance ); return;
		}
	}

	void OnMenu1( Structure.Model selection ) {

		model = selection;
		SetMenu2( );
	}

	void OnMenu2( Structure.PerformanceType selection ) {

		performanceType = selection;
		SetMenu3( );
	}

	void OnMenu3( Structure.Performance selection ) {

		performance = selection;
		SetMainScreen( );
	}

	public void Next( ) {

		var take = GetCurrentTake( );
		if( take.overrideNext ) {

			JumpToTake( take.next );
		}
		else if( take.hasJump ) {

			OnJump( );
		}
		else {

			SetTake( takeIndex + 1 );			
		}
	}

	void JumpToTake( Take target ) {

		performanceType = target.transform.parent.parent.GetComponent< Structure.PerformanceType >( );

		var jumpToPerformance = target.transform.parent.GetComponent< Structure.Performance >( );
		if( jumpToPerformance != performance ) {

			performance = jumpToPerformance;
			PushHistory( );
			SetMainScreen( );
		}

		var jumpToIndex = target.transform.GetSiblingIndex( );
		SetTake( jumpToIndex );
	}

	public void Previous( ) {

		var prev = history.Pop( );
		//this becomes previousTakeIndex so we always scroll left
		takeIndex = 1000; 
		JumpToTake( prev.take );
	}

	public void OnTake( int index ) {

		//we can only skip forward
		if( index >= takeIndex ) SetTake( index );
	}

	public void OnHome( ) {

		SetMenu1( );
	}

	public void OnDocument( Structure.Document document ) {

		activeScreenContent.SetDocument( document );
		closeButton.SetActive( true );

		foreach( var button in FindObjectsOfType< DocumentButton >( )) {

			button.MarkActive( button.document == document );
		}
	}

	public void OnClose( ) {

		var take = GetCurrentTake( );

		if( take.type == Structure.TakeType.scene ) {

			activeScreenContent.SetScene( take.scene );
		}
		else if( take.type == Structure.TakeType.document ) {

			activeScreenContent.SetDocument( take.document );
		}

		closeButton.SetActive( false );

		foreach( var button in FindObjectsOfType< DocumentButton >( )) {

			button.MarkActive( false );
		}
	}

	void Update( ) {

		if( Input.GetKeyDown( KeyCode.Escape )) {

			if( closeButton.activeInHierarchy ) OnClose( );
			else if( performanceType == null ) SetMenu1( );
			else if( performance == null ) SetMenu2( );
			else SetMenu3( );
		}

		if( Time.time - lastCheck > 5 ) {

			lastCheck = Time.time;
			var files = Directory.GetFiles( LogPath( ));
			
			foreach( var path in files ) {

				TryUploadLog( Path.GetFileName( path ));
			}
		}
	}

	void TryUploadLog( string uid ) {

		StartCoroutine( UploadLog( uid ));
	}

	IEnumerator UploadLog( string uid ) {

		var data = File.ReadAllText( Path.Combine( LogPath( ), uid ));
        var form = new WWWForm( );
        form.AddField( "file", uid );
        form.AddField( "data", data );

        var r = UnityWebRequest.Post( "http://duddels.de/amand/set.php", form );
        yield return r.SendWebRequest( );

        if( r.result != UnityWebRequest.Result.Success ) Debug.Log( r.error );
        else {

        	Debug.Log( $"upload successful: { uid }" );
        	sentUids.Add( uid );
        	File.Delete( Path.Combine( LogPath( ), uid ));
        }
    }

	public void OnConfirm( string description, string toggleInfo ) {

		var uid = currentUid;
		var n = new JSONObject( );
		n[ "description" ] = description;
		n[ "toggleInfo" ] = toggleInfo;
		n[ "model" ] = model.displayName;
		n[ "performanceType" ] = performanceType.displayName;
		n[ "performance" ] = performance.displayName;
		n[ "date" ] = DateTime.UtcNow.ToString( "o", System.Globalization.CultureInfo.InvariantCulture );
		n[ "uid" ] = uid;

		CommitLog( uid, n.ToString( ));
		TryUploadLog( uid );
	}

	void CommitLog( string uid, string data ) {

		File.WriteAllText( Path.Combine( LogPath( ), uid ), data );
		Debug.Log( $"commit successful: { uid }" );
		savedUids.Add( uid );
	}

	string GetUid( ) {

		return Guid.NewGuid( ).ToString( ) + ".json";
	}

	public void OnJump( ) {

		foreach( Transform child in activeScreenContent.toggleButtonParent ) {

			var toggleButton = child.GetComponent< ToggleButton >( );
			if( toggleButton.isOn( )) {

				JumpToTake(( toggleButton.toggle as JumpToggle ).jumpTo );
				break;
			}
		}
	}
}


}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class MenuButton : MonoBehaviour {
    
    public string menuName;
    public Object selection;
    public string primitiveSelection;
    public Text[ ] titleTexts;
    public Image image;

    public void ImplementSettings( Structure.ButtonSettings settings ) {

        if( settings != null ) {

            if( settings.sprite != null ) image.sprite = settings.sprite;    
        }
    }

    public void OnClick( ) {

        Master.Get( ).OnMenu( menuName, selection, primitiveSelection );
    }
}


}

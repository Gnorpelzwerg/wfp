using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class RenderImage : MonoBehaviour {
    
    public Image image;
    public RenderTexture texture;
    public Camera currentCamera;

    public void Detach( ) {

        foreach( var cam in FindObjectsOfType< Camera >( )) {

            if( cam.targetTexture == texture ) cam.targetTexture = null;
        }
    }

    public void Disable( ) {

        image.enabled = false;
    }

    public void Attach( Camera cam ) {

        image.enabled = true;
        currentCamera = cam;
        currentCamera.targetTexture = texture;
    }
}


}
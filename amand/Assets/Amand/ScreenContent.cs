using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class ScreenContent : MonoBehaviour {
    
    public RenderImage renderImage;
    public Transform toggleButtonParent;
    public ToggleButton toggleButtonPrefab;
    public GameObject textView;
    public Transform textParent;
    public Transform imageParent;
    public Text defaultReferenceText;
    public Image defaultReferenceImage;
    public ScrollRect textScrollRect;
    public Text title;
    public GameObject titleParent;
    public Dialog dialog;
    
    public bool initialized = false;
    public Well.WellMaster wellMaster;

    //called from animation
    public void DisableRenderImage( ) {

        renderImage.Disable( );
    }

    //called from animation
    public void DetachRenderImage( ) {

        renderImage.Detach( );
        ClearToggles( );
    }

    void ClearToggles( ) {

        Master.ClearChildren( toggleButtonParent );
    }

    //called from animation
    public void Init( ) {

        ClearToggles( );
        var group = toggleButtonParent.GetComponent< ToggleGroup >( );
        foreach( var toggle in Master.Get( ).GetCurrentTakeData( ).GetToggles( )) {

            var button = Instantiate( toggleButtonPrefab );
            button.toggle = toggle;
            button.parent = this;
            button.transform.SetParent( toggleButtonParent );
            
            if( Master.Get( ).GetCurrentTakeData( ).hasJump ) button.GetComponent< Toggle >( ).group = group;
        }

        dialog.Init( Master.Get( ).GetCurrentTakeData( ));
    }

    //reset everything here
    void Initialize( ) {

        initialized = true;
        textView.SetActive( false );
        renderImage.gameObject.SetActive( false );
        imageParent.gameObject.SetActive( false );
        SoftwareLayerSwitch.MaskEverything( );
        toggleButtonParent.gameObject.SetActive( false );
        titleParent.SetActive( false );
    }

    void Start( ) {

        if( ! initialized ) Initialize( );
    }

    public void SetScene( Well.Scene scene ) {

        Initialize( );

        renderImage.gameObject.SetActive( true );
        toggleButtonParent.gameObject.SetActive( true );
        var layerSwitch = scene.GetComponent< SoftwareLayerSwitch >( );
        if( layerSwitch ) layerSwitch.Mask( );
        
        //order important here
        wellMaster.SetScene( scene );
        renderImage.Attach( wellMaster._activeCamera );
    }

    public void SetDocument( Structure.Document document ) {

        Initialize( );

        if( document.type == Structure.DocumentType.text ) {

            textView.SetActive( true );
            var referenceText = document.hasReferenceText ? document.referenceText : defaultReferenceText;
            Master.ClearChildren( textParent );
            var text = Instantiate( referenceText, textParent );
            text.text = document.text;
            textScrollRect.content = text.GetComponent< RectTransform >( );
            textScrollRect.normalizedPosition = new Vector2( 0, 1 );
        }
        else {

            imageParent.gameObject.SetActive( true );
            var referenceImage = document.hasReferenceImage ? document.referenceImage : defaultReferenceImage;
            Master.ClearChildren( imageParent );
            var image = Instantiate( referenceImage, imageParent );
            image.sprite = document.sprite;
        }

        if( document.hasName ) {

            titleParent.SetActive( true );
            title.text = document.displayName;
        }
    }
}


}
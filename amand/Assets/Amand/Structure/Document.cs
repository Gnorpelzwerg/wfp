using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;

namespace Amand.Structure {

public enum DocumentType { text, image, reference };

public class Document : SelfReferenceable< Document > {
    
    public DocumentType type;
    [TextArea( 1, 1000 )]
    public string text;
    public Sprite sprite;
    public bool hasReferenceText;
    public bool hasReferenceImage;
    public Text referenceText;
    public Image referenceImage;
    public bool hasName;
    public string displayName;
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Document ))]
    public class DocumentInspector : SelfReferenceableInspector< Document > {

        protected override bool IsReference( Document target ) {

            Expose( "type" );
            return target.type == DocumentType.reference;
        }

        protected override void OnSelfReferenceableInspectorGUI( Document target ) {

            if( target.type == DocumentType.text ) {

            	Expose( "text" );
            	Expose( "hasReferenceText", "Has Style Reference" );

            	if( target.hasReferenceText ) {

            		Expose( "referenceText", "Style Referencee" );
            	}
            }
            else if( target.type == DocumentType.image ) {

                Expose( "sprite" );
                Expose( "hasReferenceImage", "Has Style Reference" );

                if( target.hasReferenceImage ) {

                    Expose( "referenceImage", "Style Reference" );
                }
            }

            Expose( "hasName" );
            if( target.hasName ) Expose( "displayName", "Name" );
        }
    }
    
#endif

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;

namespace Amand.Structure {


public class SelfReferenceable< T > : MonoBehaviour where T : SelfReferenceable< T > {

    public bool isReference = false;
    public T referenced = null;

    public T Follow( ) {

        if( isReference ) return referenced.Follow( );
        else return this as T; //we know that we can cast here since SelfReferenceable< T > = T
    }
}

public static class ReferenceExtensions {

    public static T[ ] GetReferencedComponentsInChildren< T >( this Component parent, bool includeInactive = false ) where T : SelfReferenceable< T > {

        var list = new List< T >( );

        foreach( var x in parent.GetComponents< T >( )) {

            list.Add( x.Follow( ));
        }

        foreach( Transform child in parent.transform ) {

            var childList = child.GetReferencedComponentsInChildren< T >( includeInactive );
            foreach( var x in childList ) list.Add( x );
        }

        return list.ToArray( );
    }

    public static T[ ] GetReferencedComponentsInChildren< T >( this GameObject parent, bool includeInactive = false ) where T : SelfReferenceable< T > {

        return parent.transform.GetReferencedComponentsInChildren< T >( );
    }
}


#if UNITY_EDITOR
    
    public abstract class SelfReferenceableInspector< T > : SmartEditor< SelfReferenceable< T >> where T : SelfReferenceable< T > {

        protected sealed override void OnSmartInspectorGUI( SelfReferenceable< T > target ) {
            
            if( IsReference( target as T )) {

                target.isReference = true;
                Expose( "referenced" );
            }
            else {

                target.isReference = false;
                OnSelfReferenceableInspectorGUI( target as T );
            }
        }

        protected virtual bool IsReference( T target ) {

            Expose( "isReference" );
            return target.isReference;
        }

        protected virtual void OnSelfReferenceableInspectorGUI( T target ) {} 
    }
    
#endif


}

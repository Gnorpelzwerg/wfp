using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using SmartEditor;
using System.Linq;

namespace Amand.Structure {


public enum TakeType { document, scene, reference };

public class Take : SelfReferenceable< Take > {
    
    public TakeType type;
    public Well.Scene scene;
    public Document document;
    public bool isImportant;
    public bool hasConfirmation;
    public bool hasOverlayText;
    [TextArea(1,100)]
    public string overlayText;
    public bool hasJump;
    public bool overrideNext;
    public Take next;
    [TextArea(1,100)]
    public string message;

    public JumpToggle[ ] GetJumpToggles( ) {

        return GetComponentsInChildren< JumpToggle >( );
    }

    public ProtocolToggle[ ] GetProtocolToggles( ) {

        return GetComponentsInChildren< ProtocolToggle >( );
    }

    public Toggle[ ] GetToggles( ) {

        return GetComponentsInChildren< Toggle >( );
    }

    public Document[ ] GetDocuments( ) {

        return this.GetReferencedComponentsInChildren< Document >( ).Where( d => type != TakeType.document || d != document ).ToArray( );
    }
}

#if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Take ))]
    public class TakeInspector : SelfReferenceableInspector< Take > {

        void ShowChildrenList( string initialText, Component[ ] children ) {

            var info = initialText + ":\n";

            foreach( var c in children ) {

                info += " - " + c.gameObject.name + "\n";
            }

            Note( children.Length > 0 ? info.Trim( ) : "No " + initialText );
        }

        protected override bool IsReference( Take target ) {

            Expose( "type" );
            return target.type == TakeType.reference;
        }

        protected override void OnSelfReferenceableInspectorGUI( Take target ) {

            if( target.type == TakeType.scene ) {

                Expose( "scene" );
            }
            else if( target.type == TakeType.document ) {

                Expose( "document" );
            }

            Expose( "isImportant" );

            if( ! target.hasJump ) Expose( "hasConfirmation" );
            
            if( target.hasConfirmation ) {

                Expose( "message" );
                ShowChildrenList( "Protocol Toggles", target.GetProtocolToggles( ));
                Space( );
            }
            
            if( ! target.hasConfirmation ) Expose( "hasJump" );

            if( target.hasJump ) {

                Expose( "message" );
                ShowChildrenList( "Jump Toggles", target.GetJumpToggles( ));
                Space( );
            }
            
            ShowChildrenList( "Additional Documents", target.GetDocuments( ));

            if( ! target.hasJump ) Expose( "overrideNext" );

            if( target.overrideNext ) {

                Expose( "next" );
            }

            Expose( "hasOverlayText" );

            if( target.hasOverlayText ) {

                Expose( "overlayText" );
            }
        }
    }
    
#endif

}
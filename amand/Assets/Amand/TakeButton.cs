using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class TakeButton : MonoBehaviour {
    
    public Text label;
    public Image image;
    public Button button;
    public int takeIndex;
    public Color colorActive;
    public Color colorInactive;
    public Color colorImportant;
    public Color colorDisabled;
    public Structure.Take take;

    void Update( ) {

        bool active = Master.Get( ).takeIndex == takeIndex;
        bool disabled = Master.Get( ).takeIndex > takeIndex;
        image.color = 

        active ? colorActive : 
            disabled ? colorDisabled : 
                take.isImportant ? colorImportant : 
                    colorInactive;

        button.interactable = ! disabled;
    }

    public void OnClick( ) {

        Master.Get( ).OnTake( takeIndex );
    }
}


}

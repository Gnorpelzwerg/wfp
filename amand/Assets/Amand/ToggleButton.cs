using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Amand {


public class ToggleButton : MonoBehaviour {
    
    public Image background;
    public Color colorOn;
    public Color colorOff;
    public Toggle uiToggle;

    public Structure.Toggle toggle;
    public ScreenContent parent;

    public bool isOn( ) {

        return uiToggle.isOn;
    }

    void Start( ) {

        uiToggle.isOn = ( toggle is Structure.ProtocolToggle ) ? 
            ( toggle as Structure.ProtocolToggle ).isOnByDefault : transform.GetSiblingIndex( ) == 0;
    }

    void Update( ) {

        var target = toggle.target;
        var cam = parent.renderImage.currentCamera;
        var screenPosition = cam.WorldToScreenPoint( target.position );
        transform.GetComponent< RectTransform >( ).anchoredPosition = screenPosition;
        background.color = isOn( ) ? colorOn : colorOff;
    }
}


}

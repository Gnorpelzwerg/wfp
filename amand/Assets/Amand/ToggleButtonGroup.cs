using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Amand {


public class ToggleButtonGroup : MonoBehaviour {
    
    public ScreenContent parent;
    
    void Update( ) {

        var list = new List< ToggleButton >( );

        foreach( Transform child in transform ) {

            list.Add( child.GetComponent< ToggleButton >( ));
        }

        var cam = parent.renderImage.currentCamera;
        var ordered = list.OrderBy( button => ( button.toggle.target.position - cam.transform.position ).magnitude ).ToArray( );

        foreach( var button in ordered ) {

            button.transform.SetSiblingIndex( 0 );
        }
    }

}


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Well {

    public class PointOfInterestSphere : MonoBehaviour {
    
    	public WellMaster _master;
        public Transform _circle;
        public PointOfInterest _follow;
        
        bool _active = false;
        
        public void SendClick( ) {
        
            _master.ReceivePointOfInterestClick( this );
        }
        
        public void Activate( ) {
        
            _active = true;
            _circle.gameObject.SetActive( true );
        }
        
        public void Deactivate( ) {
        
            _active = false;
            _circle.gameObject.SetActive( false );
        }
        
        void OnMouseDown( ) {
        
            SendClick( );
        }

        void Update( ) {
        
            if( _active ) {
            
                Vector3 fromCamera = transform.position - _master._activeCamera.transform.position;
                _circle.rotation = Quaternion.LookRotation( fromCamera );
                
                Vector3 screenCenter = _master._activeCamera.WorldToViewportPoint( transform.position );
                Vector3 worldCircleAnchor = _master._activeCamera.ViewportToWorldPoint( screenCenter + Vector3.right * _master._selectionCircleRadiusRelative );
                
                float scale = ( transform.position - worldCircleAnchor ).magnitude * 2 / transform.localScale.x;;
                _circle.localScale = Vector3.one * Mathf.Max( scale, 1.2f );
            }
        }
    }

}
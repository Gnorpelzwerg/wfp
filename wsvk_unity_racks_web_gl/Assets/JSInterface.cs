﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Well;
using SimpleJSON;

public class JSInterface : MonoBehaviour {
    
    void Start( ) {

        #if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
        #endif
    }

    public Well.Input wellInput;

    /*[DllImport( "__Internal" )]
    public static extern void Log( string msg );

    void Start( ) {

        //Log( "Test debug message" );
    }*/

    public void SetMobile( ) { SetIsMobile( 1 ); }

    public void SetDesktop( ) { SetIsMobile( 0 ); }

    public void SetIsMobile( int isMobile ) {

        wellInput._touchControls = isMobile != 0;
        Debug.Log( "Set touch controls to " + isMobile );
    }

    public void SetSensitivity( string json ) {

    	JSONNode obj = JSON.Parse( json );

    	wellInput._zoomSensitivity = obj[ 0 ].AsFloat;    	
    	wellInput._moveSensitivity = obj[ 1 ].AsFloat;
    	wellInput._touchZoomSensitivity = obj[ 2 ].AsFloat;
		wellInput._touchMoveSensitivity = obj[ 3 ].AsFloat;

		Debug.Log( "Set sensitivity to " + json );
    }
}

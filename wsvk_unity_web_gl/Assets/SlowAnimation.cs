﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowAnimation : MonoBehaviour {
    
	public new Animation animation;
	public float speed;

    void Start( ) {

    	foreach( AnimationState state in animation ){
            
            state.speed = speed;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;

namespace Well {

    public class PointOfInterest : MonoBehaviour {
    
        [Header( "User" )]
        public float _sphereRadius = 0.15f;
        public float _colliderRadius = 0.2f;
        public Color _sphereColor = Utility._orange;
        
        public bool _hasInformation = false;
        public string _name = "Point Of Interest";
        public string _text = "Text";
        public float _textBoxSizeRelative = 1;
        
        public bool _hasAlternativeCullingMask = false;
        public LayerMask _alternativeCullingMask = ~0;
        
        public bool _hasAnimation = false;
        public Animator _animatorTarget;
        
        public bool _hasAlternativePivot = false;
        public Pivot _alternativePivot;
        
        public bool _hideOthersWhenActivated;
        
        #if UNITY_EDITOR
        
        void Reset( ) {
        
            _name = gameObject.name;
        }
        
        void OnDrawGizmosSelected( ) {
            
            Utility._drawColor = Utility._orange;
            DrawSphere( _sphereRadius );
            Utility._drawColor = Color.green;
            DrawSphere( _colliderRadius );
            
            Utility._drawColor = Color.white;
            
            DrawRay( transform.up );
            DrawRay( - transform.up );
            DrawRay( transform.right );
            DrawRay( - transform.right);
            DrawRay( transform.forward );
            DrawRay( - transform.forward );
        }
        
        void DrawRay( Vector3 direction ) {
        
            Gizmos.color = Utility._drawColor;
            Gizmos.DrawRay( transform.position, direction * _colliderRadius * 5 );
        }
        
        void DrawSphere( float radius ) {
        
            Vector3 center = transform.position;
            Vector3 referenceX = center + transform.up * radius;
            Vector3 referenceY = center + transform.forward * radius;
            Vector3 referenceZ = center + transform.right * radius;
            
            Utility.DrawArcAroundAxis( center, referenceX, referenceX, transform.right, true );
            Utility.DrawArcAroundAxis( center, referenceY, referenceY, transform.up, true );
            Utility.DrawArcAroundAxis( center, referenceZ, referenceZ, transform.forward, true );
        }
        
        #endif
    }
    
    #if UNITY_EDITOR
    
    [CanEditMultipleObjects]
    [CustomEditor( typeof( PointOfInterest ))]
    public class PointOfInterestInspector : SmartEditor< PointOfInterest > {

        protected override void OnSmartInspectorGUI( PointOfInterest target ) {
            
            Expose( "_sphereRadius" );
            target._sphereRadius = Mathf.Abs( target._sphereRadius );
            
            Expose( "_colliderRadius" );
            target._colliderRadius = Mathf.Abs( target._colliderRadius );
            
            Expose( "_sphereColor" );
            Expose( "_hasInformation" );
            
            if( target._hasInformation ) {
            
                Expose( "_name" );
                Expose( "_text" );
                Expose( "_textBoxSizeRelative" );
                
                target._textBoxSizeRelative = Mathf.Clamp( target._textBoxSizeRelative, 0, 1 );
            }
            
            Expose( "_hasAlternativeCullingMask" );
            
            if( target._hasAlternativeCullingMask ) {
            
                Expose( "_alternativeCullingMask" );
                
                target._alternativeCullingMask.value |= 1 << LayerMask.NameToLayer( "UI" );
            }
            
            Expose( "_hasAnimation" );
            
            if( target._hasAnimation ) {
            
                Expose( "_animatorTarget" );
            }
            
            Expose( "_hasAlternativePivot" );
            
            if( target._hasAlternativePivot ) {
            
                Expose( "_alternativePivot" );
            }
            else {
            
                Expose( "_hideOthersWhenActivated" );
            }
        }
    }
    
    #endif
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SmartEditor;

namespace Well {

    public class Scene : MonoBehaviour {
    
        public enum CameraType { Pivot, Animated };
    
        [Header( "User" )]
        public string _displayName = "";
    	public LayerMask _cullingMask = ~0;
        public CameraType _cameraType;
        public bool _overrideCameraSettings;
        public Camera _referenceCamera;
        public PointOfInterest[ ] _pointsOfInterest;
        public Pivot _pivot;
        public Camera _animatedCamera;
        public bool _hasAnimation;
        public Animation[ ] _animationTargets;
        
        void OnDrawGizmos( ) {
    
            if( _displayName == "" ) {
                
                _displayName = name;
            }
        }
    }
    
    #if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor( typeof( Scene ))]
    public class SceneInspector : SmartEditor< Scene > {

        protected override void OnSmartInspectorGUI( Scene target ) {
            
            Expose( "_displayName" );
            Expose( "_cullingMask" );
            
            target._cullingMask.value |= 1 << LayerMask.NameToLayer( "UI" );
            
            Expose( "_cameraType" );
            
            if( target._cameraType == Scene.CameraType.Pivot ) {
            
                Expose( "_pointsOfInterest" );
                Expose( "_pivot" );
                Expose( "_overrideCameraSettings" );
                
                if( target._overrideCameraSettings ) {
                
                    Expose( "_referenceCamera" );
                    EditorGUILayout.HelpBox( new GUIContent( 
                    "Note: the reference camera's culling mask, target display and viewport rect will be ignored. For custom culling mask use the scene's culling mask instead." ));
                }
            }
            else {
            
                Expose( "_animatedCamera" );
            }
            
            Expose( "_hasAnimation" );
            
            if( target._hasAnimation ) {
            
                Expose( "_animationTargets" );
            }
        }
    }
    #endif
}

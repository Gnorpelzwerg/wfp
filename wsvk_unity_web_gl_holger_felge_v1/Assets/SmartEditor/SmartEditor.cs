﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SmartEditor {

    #if UNITY_EDITOR
    
    public abstract class SmartEditor< Type > : Editor where Type : MonoBehaviour {
    
            public sealed override void OnInspectorGUI( ) {
                
                serializedObject.Update( );
                
                OnSmartInspectorGUI( target as Type );
                
                serializedObject.ApplyModifiedProperties( );
            }
            
            bool SmartPropertyField( SerializedObject target, string name ) {
            
                return EditorGUILayout.PropertyField( target.FindProperty( name ), new GUIContent( ObjectNames.NicifyVariableName( name )), true );
            }
            
            protected bool Expose( string variableName ) {
            
                return SmartPropertyField( serializedObject, variableName );
            }
            
            protected virtual void OnSmartInspectorGUI( Type target ) {}
    }
    
    #endif
}


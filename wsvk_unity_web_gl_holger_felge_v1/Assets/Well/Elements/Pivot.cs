﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Well {
    
    public class Pivot : MonoBehaviour {
    
        [Header( "User" )]
        [Header( "Range Constraints" )]
        public float _verticalMin = -20;
        public float _verticalMax = 20;
        public float _horizontalRange = 90;
    
        [Header( "Zoom Constraints" )]
        public float _distanceMin = 5;
        public float _distanceMax = 10;
    
        [Header( "Initial Position" )]
        public float _verticalStart = 0;
        public float _horizontalStart = 0;
        public float _distanceStart = 10;
        
        public Vector3 Position( float vertical, float horizontal, float distance ) {
        
            Vector3 offset = - transform.forward * distance;
            offset = Quaternion.AngleAxis( horizontal, transform.up ) * offset;
            Vector3 axis = - Vector3.Cross( transform.up, offset );
            offset = Quaternion.AngleAxis( vertical, axis ) * offset;
            
            return transform.position + offset;
        }
        
        #if UNITY_EDITOR
        
        public void OnDrawGizmosSelected( ) {
        
            DrawOrbit( );
        }
        
        void DrawOrbit( ) {
            
            _verticalMin = Mathf.Clamp( _verticalMin, -89, _verticalMax );
            _verticalMax = Mathf.Clamp( _verticalMax, _verticalMin, 89 );
            _horizontalRange = Mathf.Clamp( _horizontalRange, 0, 360 );
            _distanceMin = Mathf.Clamp( _distanceMin, 0, _distanceMax );
            _distanceMax = Mathf.Max( _distanceMax, _distanceMin );
            
            float horizontalMin = -_horizontalRange / 2;
            float horizontalMax = _horizontalRange / 2;
            
            _verticalStart = Mathf.Clamp( _verticalStart, _verticalMin, _verticalMax );
            _horizontalStart = Mathf.Clamp( _horizontalStart, horizontalMin, horizontalMax );
            _distanceStart = Mathf.Clamp( _distanceStart, _distanceMin, _distanceMax );
            
            bool isH360 = _horizontalRange == 360;
            
            Utility._drawColor = Color.green;
            
            Utility.DrawLine( Position( _verticalMin, horizontalMin, _distanceMin ), Position( _verticalMin, horizontalMin, _distanceMax ));
            Utility.DrawLine( Position( _verticalMax, horizontalMin, _distanceMin ), Position( _verticalMax, horizontalMin, _distanceMax ));
            Utility.DrawLine( Position( _verticalMin, horizontalMax, _distanceMin ), Position( _verticalMin, horizontalMax, _distanceMax ));
            Utility.DrawLine( Position( _verticalMax, horizontalMax, _distanceMin ), Position( _verticalMax, horizontalMax, _distanceMax ));
            
            Utility.DrawArcAroundCenter( transform.position, Position( _verticalMin, horizontalMin, _distanceMin ), Position( _verticalMax, horizontalMin, _distanceMin ));
            Utility.DrawArcAroundCenter( transform.position, Position( _verticalMin, horizontalMax, _distanceMin ), Position( _verticalMax, horizontalMax, _distanceMin ));
            
            Utility.DrawArcAroundCenter( transform.position, Position( _verticalMin, horizontalMin, _distanceMax ), Position( _verticalMax, horizontalMin, _distanceMax ));
            Utility.DrawArcAroundCenter( transform.position, Position( _verticalMin, horizontalMax, _distanceMax ), Position( _verticalMax, horizontalMax, _distanceMax ));
            
            Utility.DrawArcAroundAxis( transform.position, Position( _verticalMin, horizontalMin, _distanceMin ), Position( _verticalMin, horizontalMax, _distanceMin ), transform.up, isH360 );
            Utility.DrawArcAroundAxis( transform.position, Position( _verticalMax, horizontalMin, _distanceMin ), Position( _verticalMax, horizontalMax, _distanceMin ), transform.up, isH360 );
            
            Utility.DrawArcAroundAxis( transform.position, Position( _verticalMin, horizontalMin, _distanceMax ), Position( _verticalMin, horizontalMax, _distanceMax ), transform.up, isH360 );
            Utility.DrawArcAroundAxis( transform.position, Position( _verticalMax, horizontalMin, _distanceMax ), Position( _verticalMax, horizontalMax, _distanceMax ), transform.up, isH360 );
            
            Utility.DrawLine( Position( _verticalStart, _horizontalStart, _distanceStart ), transform.position );
            Gizmos.color = Color.green;
            Gizmos.matrix = Matrix4x4.Scale( Vector3.one * 0.1f );
            Gizmos.DrawIcon( Position( _verticalStart, _horizontalStart, _distanceStart ), "start", true );
        }
        
        #endif
    }
    
    
}


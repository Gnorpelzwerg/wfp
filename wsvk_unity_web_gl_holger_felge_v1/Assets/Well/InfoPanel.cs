﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Well {

    public class InfoPanel : MonoBehaviour {
    
    	public Text _headline;
        public Text _text;
        public WellMaster _wellMaster;
        public RectTransform _textBackground;
        
        public void Close( ) {
        
            _wellMaster.ReceiveInfoPanelClose( );
        }
    }
}
